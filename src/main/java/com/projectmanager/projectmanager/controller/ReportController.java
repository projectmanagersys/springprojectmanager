package com.projectmanager.projectmanager.controller;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.ProjectRepository;
import com.projectmanager.projectmanager.repository.TaskRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;
import java.util.Set;

/**
 * Created on 22.05.2018
 *
 * @author ozgurbircan
 */
@Controller
@RequestMapping("")
public class ReportController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    TaskRepository taskRepository;

    @GetMapping("/projectReport/{id}")
    public String taskDetails(Model model, @PathVariable("id") Integer id)
    {
        Optional<Project> optionalProject=projectRepository.findById(id);
        model.addAttribute("tasks",optionalProject.get().getTasks());

        return "report";
    }
    @GetMapping("/projectUserList/{id}")
    public String projectUserList(Model model, @PathVariable("id") Integer id)
    {
        Optional<Project> optionalProject=projectRepository.findById(id);
        model.addAttribute("users",optionalProject.get().getUsers());

        return "projectUser";
    }

    @GetMapping("/allUser")
    public String allUser(Model model)
    {
        Iterable<User> users=userRepository.findAll();
        model.addAttribute("users",users);

        return "users";
    }


}
