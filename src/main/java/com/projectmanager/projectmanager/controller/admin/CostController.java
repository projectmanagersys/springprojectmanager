package com.projectmanager.projectmanager.controller.admin;

import com.projectmanager.projectmanager.model.Cost;
import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.repository.CostRepository;
import com.projectmanager.projectmanager.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("admin")
public class CostController
{
    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    CostRepository costRepository;


    @GetMapping("/addCost")
    public String addCost(Model model)
    {
        model.addAttribute("projects",projectRepository.findAll());
        model.addAttribute("cost",new Cost());
        Cost cost;

     return "admin/addCost";
    }

    @PostMapping("/addCost")
    public String addCost(@Valid @ModelAttribute Cost cost, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "admin/addCost";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));
            costRepository.save(cost);

            return "redirect:/admin";
        }

    }




}
