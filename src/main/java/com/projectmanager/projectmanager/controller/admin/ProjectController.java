package com.projectmanager.projectmanager.controller.admin;


import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.Task;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.ProjectRepository;
import com.projectmanager.projectmanager.repository.TaskRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("admin")
public class ProjectController
{
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    UserRepository userRepository;


   /* @GetMapping("/getProject")
    public ResponseEntity<List<Project>> listgetProjectResponseEntity()
    {
    //    List<Project> projects=projectRepository.findProjectByIdUser(1);

        return ResponseEntity.ok(projects);
    }
*/


    @GetMapping("/addProject")
    public String addProject(Model model)
    {
        model.addAttribute("addProject",new Project());
        return "admin/addProject";
    }

    @PostMapping("/addProject")
    public String addProject(@Valid @ModelAttribute Project project, BindingResult bindingResult, Authentication authentication)
    {
        UserDetails userDetails= (UserDetails) authentication.getPrincipal();
         Set<User> users=userRepository.findUserByUsername(userDetails.getUsername());
         Set<User> users2=userRepository.findUserByUsername("Comert");
        User user=userRepository.getBy("Comert");
        if(bindingResult.hasErrors())
        {
            return "admin/addProject";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));



            project.setUsers(users);
            project.getUsers().add(user);
            projectRepository.save(project);
            return "redirect:/admin";
        }
    }

    @GetMapping("/details")
    public String details(Model model)
    {
        model.addAttribute("userCount",userRepository.count());
        model.addAttribute("taskCount", taskRepository.count());
        model.addAttribute("projectCount",projectRepository.count());
        return "admin/details";
    }

    @GetMapping("/charts")
    public String charts()
    {
        return "charts";
    }
}
