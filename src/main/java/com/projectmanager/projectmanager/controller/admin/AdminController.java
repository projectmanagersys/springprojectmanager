package com.projectmanager.projectmanager.controller.admin;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;

@Controller
@RequestMapping("admin")

public class AdminController {

    @Autowired
    AllRepository allRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TaskRepository taskRepository;

    @Autowired
    CostRepository costRepository;

    @Autowired
    CommentRepository commentRepository;



    @GetMapping("")
    public String getIndex(Model model, @AuthenticationPrincipal User user) {
/*
        Set<Project> projects = projectRepository.findProjectsByUsers(user);
        //    model.addAttribute("projects",projects);

        System.out.println(user.getProjects());
        System.out.println(user.toString());
        model.addAttribute("projects", projects);
        */
        return "admin/index";
    }

    @GetMapping("/adminuser")
    public String getadminuser(Model model) {

        Iterable<User> users = userRepository.findUsersByEnabled(true);
        //    model.addAttribute("projects",projects);


        model.addAttribute("users", users);
        return "admin/user";
    }

    @GetMapping("/adminproject")
    public String getadminproject(Model model) {

        Iterable<Project> projects = projectRepository.findAll();
        //    model.addAttribute("projects",projects);


        model.addAttribute("projects", projects);
        return "admin/project";
    }

    @GetMapping("/widgets")
    public  String getWidgets(Model model)
    {
            model.addAttribute("totalProject",projectRepository.count());
            model.addAttribute("totalUser",userRepository.count());
            model.addAttribute("totalTask",taskRepository.count());
            model.addAttribute("totalOpenTasks",taskRepository.countTaskByTaskStatus("todo"));
            model.addAttribute("totalInProgressTasks",taskRepository.countTaskByTaskStatus("doing"));
            model.addAttribute("totalClosedTasks",taskRepository.countTaskByTaskStatus("done"));

            model.addAttribute("totalCost",costRepository.count());
            model.addAttribute("totalComment",costRepository.count());

            model.addAttribute("todayTask",taskRepository.findByCreatedTaskDate(LocalDate.now().toString()));
        return "admin/widgets";
    }




}

