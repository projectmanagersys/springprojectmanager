package com.projectmanager.projectmanager.controller.admin;

import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.AllRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Created on 25.03.2018
 *
 * @author ozgurbircan
 */

@Controller
@RequestMapping("admin")
public class UserEditController
{
    @Autowired
    AllRepository allRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;

    @PostMapping("/deleteuser/{id}")
    public String deleteUser(@PathVariable("id") Integer id){

        Optional<User> entryOptional= userRepository.findById(id);
        entryOptional.get().setEnabled(false);

        if(!entryOptional.isPresent())
        {

            return "redirect:/admin";
        }
        else
        {
            userRepository.save(entryOptional.get());
            return "redirect:/admin/adminuser";

        }
    }

    @PostMapping("/update/{id}")
    public String updateuser(@PathVariable(value = "id") Integer userid,@Valid @ModelAttribute User user) {

        Optional<User> entryOptional= userRepository.findById(userid);
        entryOptional.get().setUsername(user.getUsername());
        entryOptional.get().setPassword(passwordEncoder.encode(user.getPassword()));
        entryOptional.get().setEmail(user.getEmail());
        entryOptional.get().setRole(user.getRole());
        entryOptional.get().setGitAccount(user.getGitAccount());


        if(!entryOptional.isPresent())
        {
            return "redirect:/admin";
        }
        else
        {
            userRepository.save(entryOptional.get());
            return "redirect:/admin";

        }


    }

    @GetMapping("/update/{id}")
    public String getupdate(Model model, @PathVariable(value = "id") Integer userid) {
        Optional<User> entryOptional= userRepository.findById(userid);

        String[] roles={"ROLE_GENERALUSER","ROLE_MANAGER","ROLE_TESTER","ROLE_FRONTEND","ROLE_BACKEND","ROLE_ADMIN"};
        model.addAttribute("user", entryOptional.get());
        model.addAttribute("roles", roles);

        return "admin/update";
    }
}
