package com.projectmanager.projectmanager.controller.admin;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.Task;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.ProjectRepository;
import com.projectmanager.projectmanager.repository.TaskRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import com.sun.corba.se.impl.ior.iiop.IIOPProfileTemplateImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("")
public class TaskController
{

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/projectDetails/{id}")
    public String getDetails(@PathVariable("id") Integer id, Model model)
    {
        Project project=projectRepository.findProjectByIdProject(id);



        List<Task> tasktodo=taskRepository.findTaskByProjectAndTaskStatus(project,"todo");
        List<Task>taskdoing=taskRepository.findTaskByProjectAndTaskStatus(project,"doing");
        List<Task> taskdone=taskRepository.findTaskByProjectAndTaskStatus(project,"done");

        Iterable<User> userList=userRepository.findAll();

        String [] status={"todo","doing","done"};
        model.addAttribute("status",status);
        model.addAttribute("project",project);
        model.addAttribute("tasktodo",tasktodo);
        model.addAttribute("taskdoing",taskdoing);
        model.addAttribute("taskdone",taskdone);
        model.addAttribute("users",userList);
        model.addAttribute("addTask",new Task());
        model.addAttribute("user",new User());
        return "projectDetails";
  }


    @PostMapping("/addTask/{id}")
    public String addProject(@PathVariable("id") Integer id,@Valid @ModelAttribute Task task, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "projectDetails";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));
            task.setProjectIdProject(id);
            taskRepository.save(task);


            return "redirect:/projectDetails/{id}";
        }
    }


    @PostMapping("/inProgress")
    public String editTask(@Valid @ModelAttribute Task task, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "projectDetails";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));
            Optional<Task> taskOptional=taskRepository.findById(task.getIdTask());
            taskOptional.get().setTaskStatus("doing");
            taskRepository.save(taskOptional.get());
            int pi=taskOptional.get().getProjectIdProject();

            return "redirect:/projectDetails/"+pi;
        }
    }

    @PostMapping("/done")
    public String doneTask(@Valid @ModelAttribute Task task, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "projectDetails";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));
            Optional<Task> taskOptional=taskRepository.findById(task.getIdTask());
            taskOptional.get().setTaskStatus("done");
            taskRepository.save(taskOptional.get());
            int pi=taskOptional.get().getProjectIdProject();

            return "redirect:/projectDetails/"+pi;
        }
    }

    @PostMapping("/addUserProject/{id}")
    public String addUserProject(@PathVariable("id") Integer id,@Valid @ModelAttribute User user, BindingResult bindingResult)
    {
        user=userRepository.getBy(user.getUsername());
        Project project=projectRepository.findProjectByIdProject(id);
        if(bindingResult.hasErrors())
        {
            return "projectDetails";
        }
        else
        {
            //project.setIdUser(userRepository.findUserByUsername(userDetails.getUsername()));


            project.getUsers().add(user);
            projectRepository.save(project);

            return "redirect:/projectDetails/{id}";
        }
    }


}
