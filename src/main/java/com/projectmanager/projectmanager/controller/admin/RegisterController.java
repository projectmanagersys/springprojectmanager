package com.projectmanager.projectmanager.controller.admin;

import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.service.UserCheckService;
import com.projectmanager.projectmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created on January, 2018
 *
 * @author adilcan
 */
@Controller
@RequestMapping("")
public class RegisterController {

	@Autowired
	private UserService userService;

	@Autowired
	UserCheckService userCheckService;

	@GetMapping("/register")
	public String register(Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {

			return "redirect:/index";
		}
		else
		{

			model.addAttribute("user", new User());
			return "register";
		}
	}

	@PostMapping("/register")
	public String register(@ModelAttribute User user,RedirectAttributes redirectAttributes) {

		boolean check = userCheckService.emailAndUsernameCheck(user);
		if (check == true) {
			redirectAttributes.addFlashAttribute("error", "Email adresi veya username kullanılıyor.");
			return "redirect:/register";
		}
		else {
			userService.register(user);
			return "redirect:/";
		}
	}



}
