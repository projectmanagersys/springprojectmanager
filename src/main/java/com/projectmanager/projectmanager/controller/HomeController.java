
package com.projectmanager.projectmanager.controller;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.AllRepository;
import com.projectmanager.projectmanager.repository.ProjectRepository;
import com.projectmanager.projectmanager.repository.TaskRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

@Controller
@RequestMapping("")
public class HomeController {



    @Autowired
    AllRepository allRepository;
    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TaskRepository taskRepository;

    @GetMapping("/login")
    public String getLoginPage(){
        return "login";
    }

    @GetMapping("/403")
    public String accessDeniedPage()
    {
        return "403";
    }

@GetMapping("")
public String getHome()
{
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)) {

        return "redirect:/index";
    }
    else
    return "homepage";

}

    @GetMapping("/index")
    public String getIndex(Model model,@AuthenticationPrincipal User user)
    {

        Set<Project> projects=projectRepository.findProjectsByUsers(user);
    //    model.addAttribute("projects",projects);

        System.out.println(user.getProjects());
        System.out.println(user.toString());
        model.addAttribute("projects",projects);
        return "index";
    }







  /*  @GetMapping("/getTask")
    public ResponseEntity<List<Task>> listgetTaskResponseEntity()
    {
        List<Task> tasks=taskRepository.findTaskByIdProject(1);

        return ResponseEntity.ok(tasks);
    }
*/










}