package com.projectmanager.projectmanager.controller;


import com.projectmanager.projectmanager.model.Task;
import com.projectmanager.projectmanager.repository.CommentRepository;
import com.projectmanager.projectmanager.repository.TaskRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class CommentController {

    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TaskRepository taskRepository;


    @GetMapping("/taskDetails/{id}")
    public String taskDetails(Model model, @PathVariable("id") Integer id)
    {
        model.addAttribute("comments",commentRepository.findCommentsByTaskIdTask(id));

        return "commentDetails";
    }
}
