package com.projectmanager.projectmanager.controller;

import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class UploadController {

    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "/usr/share/nginx/html/images";
    @Autowired
    UserRepository userRepository;
    private Environment env;
    @GetMapping("/uploadfile")

    public String index() {
        return "uploadFile";
    }

    @PostMapping("/uploadfile")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes,@AuthenticationPrincipal User user)
    {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/myprofile";
        }

        try {

            System.out.println(file.getContentType());
            String fileName=file.getOriginalFilename();
            String newFileName=java.util.UUID.randomUUID().toString().replace("-",
                    "")+".png";

            System.out.println(fileName);
            //System.out.println(newFileName);

            String filename = file.getOriginalFilename();
            String filepath = Paths.get(UPLOADED_FOLDER, newFileName).toString();

            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            stream.write(file.getBytes());
            stream.close();

            user.setProfileUrl("http://18.196.119.4:5353/images/"+newFileName);
            userRepository.save(user);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded ");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/myprofile";
    }

}