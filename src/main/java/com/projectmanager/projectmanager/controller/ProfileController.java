package com.projectmanager.projectmanager.controller;

import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Created on 25.03.2018
 *
 * @author ozgurbircan
 */
@Controller
@RequestMapping("")
public class ProfileController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/editUser/{id}")
    public String editUser(@PathVariable(value = "id") Integer userid, @Valid @ModelAttribute User user,
                           RedirectAttributes redirectAttributes) {

        Optional<User> entryOptional = userRepository.findById(userid);
        entryOptional.get().setUsername(user.getUsername());
        entryOptional.get().setPassword(passwordEncoder.encode(user.getPassword()));
        entryOptional.get().setEmail(user.getEmail());
        entryOptional.get().setGitAccount(user.getGitAccount());


        if (!entryOptional.isPresent()) {
            redirectAttributes.addFlashAttribute("pwmessage", "Error");
            return "redirect:/myprofile";
        } else {
            redirectAttributes.addFlashAttribute("pwmessage", "Your password has been changed successfully! ");

            userRepository.save(entryOptional.get());
            return "redirect:/myprofile";

        }


    }
}