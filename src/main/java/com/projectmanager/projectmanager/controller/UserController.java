package com.projectmanager.projectmanager.controller;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.Task;
import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.AllRepository;
import com.projectmanager.projectmanager.repository.UserRepository;
import com.projectmanager.projectmanager.service.UserService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("")
public class UserController {

    @Autowired
    AllRepository allRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;



    @GetMapping("/myprofile")
    public String getMyProfile(Model model, @AuthenticationPrincipal User user)
    {
        model.addAttribute("user",user);
        model.addAttribute("profileImage",user.getProfileUrl());
        return "myprofile";
    }

    @GetMapping("/myTasks")
    public String getMyTasks(Model model,@AuthenticationPrincipal User user)
    {
        Optional<User> userOptional=userRepository.findByUsername(user.getUsername());
        System.out.println(user.getTasks());
        model.addAttribute("myTasks",userOptional.get().getTasks());
        return "myTasks";
    }

}





