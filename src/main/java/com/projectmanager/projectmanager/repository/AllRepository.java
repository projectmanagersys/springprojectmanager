package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AllRepository extends CrudRepository<User,Integer>
{
   // @Query("select p from User p")
  //  List<User> findAllP();
}
