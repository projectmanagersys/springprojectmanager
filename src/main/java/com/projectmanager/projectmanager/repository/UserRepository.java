package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends CrudRepository<User,Integer> {

   // String getUserByIdUserAndEmail(Integer id,String email);
   Optional<User> findByUsername(String username);


    @Query("SELECT u FROM User u WHERE u.username = :username")
    Set<User> findUserByUsername(@Param("username") String username);

    Integer findUserByIdUser(Integer id);

    @Query("SELECT u FROM User u WHERE u.username = :username")
    User getBy(@Param("username") String username);

    @Modifying
    @Query("DELETE FROM  User u Where u.idUser=:id")
     void deleteBy(@Param("id") Integer id);


    @Query("SELECT u FROM User u ")
    Set<User> findAllByUsers();

    @Query("update User u set u.enabled=0 where u.idUser=:id")
    void userDisabled(@Param("id") Integer id);

     @Query("SELECT u FROM User u ")
     List<User> findAllUser();

    Iterable<User> findUsersByEnabled(boolean b);

    boolean existsByUsername(String username);
    boolean existsByEmail(String email);

}

