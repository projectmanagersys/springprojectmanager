package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.Cost;
import org.springframework.data.repository.CrudRepository;

public interface CostRepository  extends CrudRepository<Cost,Integer>{

}
