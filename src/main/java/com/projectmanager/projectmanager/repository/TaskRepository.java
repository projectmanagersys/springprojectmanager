package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.Task;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task,Integer> {

   // List<Task> findTasksByIdProject(Integer id);

   // List<Task> findTaskByProjectAndTaskStatus(Project project, String taskStatus);

    List<Task> findTaskByProjectAndTaskStatus(Project project,String taskStatus);

    List<Task> findTasksByProject(Project project);

    List<Task> findByCreatedTaskDate(String s);

    Integer countTaskByTaskStatus(String status);

}
