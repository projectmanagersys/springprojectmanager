package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.Project;
import com.projectmanager.projectmanager.model.Task;
import com.projectmanager.projectmanager.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProjectRepository extends CrudRepository<Project,Integer> {

  //  List<Project> findProjectByIdUser(Integer id);

   // String findProjectByBudgetGreaterThan(Integer bugdet);

    Set<Project> findProjectsByUsers(User user);

    //List<Project> findProjectBy_idUser(Integer id);
    Project findProjectByIdProject(Integer id);


}
