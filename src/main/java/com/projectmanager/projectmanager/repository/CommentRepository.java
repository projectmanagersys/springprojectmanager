package com.projectmanager.projectmanager.repository;

import com.projectmanager.projectmanager.model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment,Integer> {

    List<Comment> findCommentsByTaskIdTask(Integer id);


}
