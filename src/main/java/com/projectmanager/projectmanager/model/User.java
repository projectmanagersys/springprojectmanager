package com.projectmanager.projectmanager.model;
// Generated Feb 17, 2018 6:35:30 PM by Hibernate Tools 5.2.3.Final

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * User generated by hbm2java
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "User", catalog = "pmi")
public class User implements UserDetails{





	@Email
	private String email;

	@Column
	private String fullName;
	@Column
	private String profileUrl="http://18.196.119.4:5353/images/9a2ebbdfc96c4fd8ad8fdadc404893a2.png";

	private String password;

	@Column
	private boolean enabled = true;

	private String role = "ROLE_USER";

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idUser", unique = true, nullable = false)
	private Integer idUser;


	@Column(name = "username", length = 45)
	private String username;

	@Column(name = "gitAccount", length = 300)
	private String gitAccount;

	@ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinTable(name = "User_has_Project", catalog = "pmi",
			joinColumns =
			@JoinColumn(name = "idUser"),
			inverseJoinColumns =
			@JoinColumn(name = "idProject"))
	private Set<Project> projects = new HashSet<Project>();



	//@ManyToMany(fetch = FetchType.LAZY,mappedBy = "users")
	//private Set<Project> projects=new HashSet<Project>();


	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Task> tasks = new HashSet<Task>(0);



	@Override
	public String toString() {
		return "User{" +
				"email='" + email + '\'' +
				", password='" + password + '\'' +
				", isEnabled=" + enabled +
				", role='" + role + '\'' +
				", idUser=" + idUser +
				", username='" + username + '\'' +
				", projects=" + projects +
				", userCategories=" + userCategories +
				'}';
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserCategory> userCategories = new HashSet<UserCategory>(0);

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(this.role));
		return authorities;
	}






	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
