package com.projectmanager.projectmanager.model;
// Generated Feb 19, 2018 3:09:30 AM by Hibernate Tools 5.2.3.Final

import javax.persistence.*;

/**
 * TaskId generated by hbm2java
 */
public class TaskId implements java.io.Serializable {

	private int idTask;
	private int projectIdProject;
	private int userIdUser;

	public TaskId() {
	}

	public TaskId(int idTask, int projectIdProject, int userIdUser) {
		this.idTask = idTask;
		this.projectIdProject = projectIdProject;
		this.userIdUser = userIdUser;
	}


	@Column(name = "idTask")
	public int getIdTask() {
		return this.idTask;
	}

	public void setIdTask(int idTask) {
		this.idTask = idTask;
	}

	@Column(name = "Project_idProject", nullable = false)
	public int getProjectIdProject() {
		return this.projectIdProject;
	}

	public void setProjectIdProject(int projectIdProject) {
		this.projectIdProject = projectIdProject;
	}

	@Column(name = "User_idUser", nullable = false)
	public int getUserIdUser() {
		return this.userIdUser;
	}

	public void setUserIdUser(int userIdUser) {
		this.userIdUser = userIdUser;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TaskId))
			return false;
		TaskId castOther = (TaskId) other;

		return (this.getIdTask() == castOther.getIdTask())
				&& (this.getProjectIdProject() == castOther.getProjectIdProject())
				&& (this.getUserIdUser() == castOther.getUserIdUser());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getIdTask();
		result = 37 * result + this.getProjectIdProject();
		result = 37 * result + this.getUserIdUser();
		return result;
	}

}
