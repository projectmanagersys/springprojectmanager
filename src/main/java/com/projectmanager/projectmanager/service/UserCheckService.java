package com.projectmanager.projectmanager.service;

import com.projectmanager.projectmanager.model.User;
import com.projectmanager.projectmanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 25.03.2018
 *
 * @author ozgurbircan
 */

@Service
public class UserCheckService {
    @Autowired
    UserRepository userRepository;
    boolean username;
    boolean email;

    public boolean emailAndUsernameCheck(User user)
    {
        username=userRepository.existsByUsername(user.getUsername());
        email=userRepository.existsByEmail(user.getEmail());

        if (username==true || email==true)
            return true;

        return false;
    }
}
